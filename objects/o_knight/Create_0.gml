event_inherited();

initial_state = "chasing";
dead_sprite = s_knight_die_strip6
knockback_sprite = s_knight_hitstun;

state = initial_state; 

movement_spd = 1;
attack_range = 35;
hp = 10;

attack_knockback = 3;
attack_lifespan = 4;
attack_damage = 10;
