event_inherited();

function calculate_distance_to_player() {
	return point_distance(o_skeleton.x, o_skeleton.y, x, y);
}

function is_close_enough_to_attack() {
	return attack_range >= calculate_distance_to_player();
}

function chase_state() {	
	set_state_sprite(s_knight_walk_strip4, 0.25, 0);
	
	if not instance_exists(o_skeleton) {
		return;
	};
	
	image_xscale = sign(o_skeleton.x - x);
	
	if(image_xscale == 0) {
		image_xscale = -1;
	}
	
	if is_close_enough_to_attack() {
		state = "attacking";
	} else {
		move_and_collide(movement_spd, image_xscale, 0, 0, o_wall);
	}
}

function attack_state() {	
	set_state_sprite(s_knight_attack_strip12, 0.4, 0);
	
	if animation_hit_frame(4) {
		create_hitbox(x, y, image_xscale, self, s_skeleton_attack_one_damage, attack_knockback, attack_lifespan, attack_damage);
	}
	
	if animation_end() {
		state = "chasing"
	}
}

switch (state) {
	case "chasing":
		chase_state();
		break;
	case "attacking":
		attack_state();
		break;
	case "dead":
		dead_state();
		break;
}