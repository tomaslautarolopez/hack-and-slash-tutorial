event_inherited();

function move_state() {
	var dir = 0;

	if input.right {
		dir = 1;
		image_xscale = 1;
		set_state_sprite(s_skeleton_run_strip6, 0.4, 0);
	}

	if input.left {
		dir = -1;
		image_xscale = -1;
		set_state_sprite(s_skeleton_run_strip6, 0.4, 0);
	};

	if not input.right and not input.left  {
		set_state_sprite(s_skeleton_idle_strip3, 0.25, 0);
	}

	if input.roll {
		state = "rolling";
		image_index = 0;
	}
	
	if input.attack {
		state = "attacking_one";
		image_index = 0;
	}
	
	move_and_collide(movement_spd, dir , 0, 0, o_wall);
}

function roll_state() {	
	set_state_sprite(s_skeleton_roll_strip7, 0.5, 0);

	move_and_collide(rolling_spd, image_xscale, 0, 0, o_wall);
	
	if animation_end() {
		state = "able_to_move"
	}
}

function attack_one_state() {	
	set_state_sprite(s_skeleton_attack_one_strip5, 0.5, 0);
	
	if animation_hit_frame(0) {
		create_hitbox(x, y, image_xscale, self, s_skeleton_attack_one_damage, attack_one_knockback, attack_one_lifespan, attack_one_damage);
	}
	
	if input.attack and animation_hit_frame_range(2, 4) {
		state = "attacking_two";
	}
	
	if animation_end() {
		state = "able_to_move"
	}
}

function attack_two_state() {
	set_state_sprite(s_skeleton_attack_two_strip5, 0.5, 0);
	
	if animation_hit_frame(1) {
		create_hitbox(x, y, image_xscale, self, s_skeleton_attack_two_damage, attack_two_knockback, attack_two_lifespan, attack_two_damage);
	}
	
	if input.attack and animation_hit_frame_range(2, 4) {
		state = "attacking_three";
	}

	if animation_end() {
		state = "able_to_move"
	}
}

function attack_three_state() {
	set_state_sprite(s_skeleton_attack_three_strip6, 0.5, 0);

	if animation_hit_frame(2) {
		create_hitbox(x, y, image_xscale, self, s_skeleton_attack_three_damage, attack_three_knockback, attack_three_lifespan, attack_three_damage);
	}
	
	if animation_end() {
		state = "able_to_move"
	}
}

switch state {
	case "able_to_move":
		move_state();
		break;
	case "rolling":
		roll_state();
		break;
	case "attacking_one":
		attack_one_state();
		break;
	case "attacking_two":
		attack_two_state();
		break;
	case "attacking_three":
		attack_three_state();
		break;
	case "dead":
		dead_state();
		break;
}