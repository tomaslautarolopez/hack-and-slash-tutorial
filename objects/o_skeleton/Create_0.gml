/// @description Insert description here
event_inherited();

hp_max = 30;
hp = hp_max;

initial_state = "able_to_move";
dead_sprite = s_skeleton_bones_strip10;
knockback_sprite = s_skeleton_hitstun;

image_speed = 0.25;

state = initial_state;
movement_spd = 3.5; 
rolling_spd = 3.5;

input = instance_create_layer(0,0, "Instances", o_input);

attack_one_knockback = 2;
attack_one_lifespan = 4;
attack_one_damage = 1;

attack_two_knockback = 3;
attack_two_lifespan = 2;
attack_two_damage = 2;

attack_three_knockback = 5;
attack_three_lifespan = 1;
attack_three_damage = 5;