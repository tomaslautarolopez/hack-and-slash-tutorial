var ly_CloseGravesBackground_id = layer_get_id("CloseGravesBackground");
var bg_CloseGravesBackground_id = layer_background_get_id(ly_CloseGravesBackground_id);

var ly_FarGravesBackground_id = layer_get_id("FarGravesBackground");
var bg_FarGravesBackground_id = layer_background_get_id(ly_FarGravesBackground_id);

layer_background_blend(bg_CloseGravesBackground_id, close_gray);
layer_background_blend(bg_FarGravesBackground_id, far_gray);
