if creator == noone or creator == other or ds_list_find_index(hitted, other) != -1 {
	exit;
}

ds_list_add(hitted, other)
other.transition_to_knockback(knockback, image_xscale);
other.hp = max(other.hp - damage, 0);
