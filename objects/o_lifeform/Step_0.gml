function dead_state() {
	set_state_sprite(dead_sprite, 0.4, 0);
	
	if animation_end() {
		image_speed = 0;
		self.alarm[0] = delay_til_destroy;
	}
}

function transition_to_knockback(knck, knck_dir) {
	if state != "knockback" and state != "dead" {
		knockback = knck;
		knockback_dir = knck_dir
		state = "knockback";
	}
}

function knockback_state() {
	set_state_sprite(knockback_sprite, 0.4, 0);
	move_and_collide(knockback, knockback_dir, 0, 0, o_wall);
	knockback = lerp(knockback, 0, 0.1)
		
	if knockback < 1 {
		knockback = 0;
		state = initial_state;
	}
}

if(state = "knockback") {
	knockback_state();
}

if (hp <= 0) {
	state = "dead"
};