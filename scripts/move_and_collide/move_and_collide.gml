// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function move_and_collide(spdX, dirX, spdY, dirY, object){
	var new_pos_x = x + (spdX * dirX);
	var new_pos_y = y + (spdY * dirY);
	
	if place_meeting(new_pos_x, y, object) {
		new_pos_x = x;
	}
	
	if place_meeting(x, new_pos_y, object) {
		new_pos_y = y;
	}
	
	x = new_pos_x;
	y = new_pos_y;
}
