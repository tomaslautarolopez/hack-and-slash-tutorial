// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function create_hitbox(xCord, yCord, xScale, creator, sprite, knockback, lifespan, damage) {
	var hitbox = instance_create_layer(xCord, yCord, "Instances", o_hitbox);
	hitbox.sprite_index = sprite;
	hitbox.creator = creator;
	hitbox.knockback = knockback;
	hitbox.alarm[0] = lifespan;
	hitbox.damage = damage;
	hitbox.image_xscale = xScale;
}