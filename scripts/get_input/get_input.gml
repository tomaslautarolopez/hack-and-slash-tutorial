// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function get_input(){
	right = keyboard_check(vk_right) || gamepad_button_check(0, gp_padr);
	left = keyboard_check(vk_left) || gamepad_button_check(0, gp_padl);
	roll = keyboard_check_pressed(vk_space) || gamepad_button_check_pressed(0, gp_face1);
	attack = keyboard_check_pressed(ord("A")) || gamepad_button_check_pressed(0, gp_face3);
}